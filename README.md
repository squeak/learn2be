# learn2be

Learn2be is an extension of the [grav learn2 theme](https://github.com/getgrav/grav-theme-learn2), removing Google dependencies and modifying a few of it's behaviours.

Here is a list of the main improvements provided:
  - Removed dependency on Google fonts
  - You can customize the logo image (in sidebar), for that add it in your pages at the following location: `/images/logo.png`
  - You can customize the favicon, for that add it in your pages at the following location: `/images/favicon.png`
  - Fixed arrows not working if you don't define a docs category taxonomy for your pages. Now all pages are considered. If you don't want all pages to be considered, just set `taxonomy: category: docs` in your pages.

## Install

To install and use this theme, you'll have to follow the following steps:
  - Install [learn2](https://github.com/getgrav/grav-theme-learn2) theme (with gpm, admin or from it's repository)
  - Install this theme: download the repository, and put it in your themes folder
  - Set your site to use learn2be theme (from admin or in `config/system.yaml` file)
  - Add a symbolic link to `learn2/scss` folder in learn2be and name it `scss-learn2`:
    ```
      cd user/themes/learn2be/
      ln -s ../learn2/scss/ scss-learn2
    ```
  - Build styles (if you don't have `sass` installed, you'll have to [install it](https://sass-lang.com/install/) before doing this):
    ```
      cd user/themes/learn2be/
      bash scss.sh
    ```
  - That's it you're up and running and your site should now be working as it was with `learn2`, just with a few tweaks.
  - If you want to modify sidebar logo or favicon, follow the instructions in the previous section.
