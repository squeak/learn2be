<!--
# v1.2.0
## 2056-07-29

1. [](#new)
    * New features added
    * Another new feature
2. [](#improved)
    * Improvement made
    * Another improvement
3. [](#bugfix)
     * Bugfix implemented
     * Another bugfix -->


# v0.0.3
## 2024-04-14

1. [](#improved)
    * Improved scss script.

# v0.0.2
## 2024-04-09

1. [](#new)
    * Added install instructions in README.
    * Created this changelog file.

# v0.0.1
## 2024-04-08

1. [](#new)
    * Created learn2be theme.
