#!/bin/bash

# get and go to current script path
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"

# build dists and watch changes
sass --watch scss:css-compiled

